/*! beardJS v1.0.1 | (c) 20014 
**! Dimitri Gigot - dim@dim-dim.be
**! https://bitbucket.org/dimdimbe/beardjs
**! test git post 
*/
if(typeof $$=='undefined')console.warn('quoJS are required');
if(typeof Mustache=='undefined')console.warn('Mustache is required');
(function($$){ 
    $$.tplList=[];
    
    $$.beard_data={};
    
    $$.fn.render = function(data,callback){
        var selector = this.selector;
        return this.each(function(){ 
           var s = $$(this);
            var tpl;
            if($$.tplList.indexOf(selector)==-1){
                tpl =s.html() ;
                $$.tplList.push(selector,tpl);
            }else{
                tpl=$$.tplList[$$.tplList.indexOf(selector)+1];
            }
            
            s.html(Mustache.render(tpl,data));
            if(typeof callback != 'undefined')callback(s);
        });
    };
    
    $$.fn.setBeardObject = function(data,callback){
        
        return this.each(function(){ 
            var this_id = $$(this)[0].dataset.beardId;
            if(typeof callback!='undefined'){
                $$.setBeardObjectTo(this_id,data,callback);
            }else{
                $$.setBeardObjectTo(this_id,data);
            }
            
        });
    };    
    
    $$.fn.setBeardAttribute = function(data,callback){
        
        return this.each(function(){ 
            var this_id = $$(this)[0].dataset.beardId;
            if(typeof callback!='undefined'){
                $$.setBeardAttributeTo(this_id,data,callback);
            }else{
                $$.setBeardAttributeTo(this_id,data);
            }
            
        });
    };    

    $$.setBeardObjectTo = function(id,data,callback){
       var databearid=$$('[data-beard-id="'+id+'"]');
       $$.beard_data[id]=data;
        databearid.render($$.beard_data[id]);
       if(typeof callback != 'undefined')callback(databearid);
    };
    
    $$.setBeardAttributeTo = function(id,data,callback){
        var databearid=$$('[data-beard-id="'+id+'"]');
        var list_keys = Object.keys(data);
        var beard = $$.beard_data[id];
        for(var i=0; i<list_keys.length;i++){
            beard[list_keys[i]]=data[list_keys[i]];
        }
        databearid.render($$.beard_data[id]);
        
       if(typeof callback != 'undefined')callback(databearid);
    };
})($$);